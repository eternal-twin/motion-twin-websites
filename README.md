
Use this repository to store the assets from the central sites of Motion Twin (Twinoid...).

Don't store game-specific elements here. Store them in the dedicated repository of the game in  https://gitlab.com/eternal-twin
