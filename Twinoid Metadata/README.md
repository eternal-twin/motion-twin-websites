
The metadata of all the games of Twinoid, with name/description/screenshots/game cover. All those elements are linked by a home-made JSON file ("game.json" placed in each folder). It will allow automatic extractions later if needed. Sample in /games/alphabounce/game.json

There are duplicate files in the screenshots, not cleaned up yet (january 2020)

Done by Moulins/EternalGorithm (Discord: moulins#3848)

